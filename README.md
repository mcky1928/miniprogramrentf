## 小程序 消费者版

### 目录结构：

	app.js
		小程序逻辑处理，公用方法
	app.json
		小程序公共配置
	app.wxss
		小程序公共样式表
	/pages
		小程序页面，每个页面由4部分组成 js, wxml, json, wxss
	/imgaes
		小程序内图片文件
	/untils
		外部引用插件，及过滤器
	project.config.json
		项目配置文件
	

### 内容说明：

首先打开消费者版小程序需用户登录

1. 微信登录 - 绑定手机号 授权 pages/login/login
2. 手机号验证码登录 - 绑定当前微信 pages/login/phoneLogin

登录之后可进入小程序 正常使用 

消费者版  主要内容很简单
主要包括用户扫码开机，和用户个人中心 两大模块

租赁模块：pages/index/index

1. 首页，定位  在地图上显示商家分布 pages/index/index

	点击店铺图标可查看商家地址 ，选择导航等操作
	用户也可使用地图上面的搜索 查看店铺列表 或根绝关键字搜索指定商家

2. 扫码开锁 pages/shop/shop
	
	用户扫码设备二维码 进入到设备详情页面 pages/shop/shop
	可以查看该设备的店铺信息 pages/storeInfo/storeInfo
	及设备的信息与租赁价格 
	
	确定支付 便可开机使用 完成租赁 pages/payPage/payPage

个人中心：pages/personal/personal

1. 我的钱包 pages/wallet/wallet
	
	1) 查看当前余额
	2) 充值 pages/recharge/recharge
	3) 提现 pages/withdrawal/withdrawal
		可查看提现记录 pages/withdrawalHistory/withdrawalHistory
		可查看不可提现额度（主要包括租赁设备的押金及设备损坏需返厂维修两种情况） pages/notWithdrawal/notWithdrawal
		
2. 我的订单 pages/myOrder/myOrder

	包括 进行中 和 已完成 两个选项
	查看订单列表 
	及订单详情 pages/myOrderDetails/myOrderDetails

3. 获取帮助 pages/help/help

	帮助页面提供一些常见的问题及介绍，以方便用户使用
	
	
其他：
1. 阿里图标库：utils/iconfont.wxss
2. 公用过滤器文件：utils/filter/filter.wxs


END!
		
