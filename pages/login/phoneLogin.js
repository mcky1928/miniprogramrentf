// pages/login/phoneLogin.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userType: '1',
    showZqmodal: false,//modal显示隐藏
    phonenumber: "",//手机号
    code: "",//验证码
    isCountdown: false,//是否开始倒数
    count: 30,//倒数时间
    setInter: "",//定时器
    resData: {},
    wxcode: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  wxLogin: function () {
    let self = this;
    wx.showLoading({
      title: '加载中'
    });
    wx.login({
      success(res) {
        //console.log(res)
        wx.hideLoading({});
        self.setData({
          wxcode: res.code
        })
      }
    })
  },
  getCode: function () {
    let self = this;
    let phonenumber = self.data.phonenumber;
    //console.log(phonenumber)
    if (!(/^1[345789]\d{9}$/.test(phonenumber))) {
      wx.showToast({
        title: '手机号码有误，请重输',
        icon: 'none',
        duration: 2000
      })
      return false;
    } else {
      let url = app.globalData.Host + '/register/getCaptcha';
      let data = {
        phone: phonenumber
      }
      app.globalData.get(url, data,
        function (res) {
          wx.showToast({
            title: res.message,
            icon: 'none',
            duration: 2000
          })
          self.countdownStart();
        },
        function (err) {
        }
      )
    }
  },
  phoneLogin: function () {
    let self = this;
    let phonenumber = this.data.phonenumber;
    let code = this.data.code;
    if (!(/^1[345789]\d{9}$/.test(phonenumber))) {
      wx.showToast({
        title: '手机号码有误，请重输',
        icon: 'none',
        duration: 2000
      })
      return false;
    } else if (!code) {
      wx.showToast({
        title: '请输入验证码',
        icon: 'none',
        duration: 2000
      })
      return false;
    } else {
      let url = app.globalData.Host + '/login/doLogin';
      let data = {
        phone: phonenumber,
        captcha: code,
        type: self.data.userType
      }
      app.globalData.post(url, data,
        function (res) {
          let resData = res.data;
          self.setData({
            resData: res.data
          })
          console.log(self.data.resData)
          if (resData.uuid) {
            wx.setStorage({
              key: 'user',
              data: resData
            })
            app.globalData.userInfo = resData;
            wx.redirectTo({
              url: '/pages/index/index',
            })
          } else {
            self.setData({ showZqmodal: true });
          }
        },
        function (err) {
        }
      )
    }
  },
  getUserInfo: function (e) {
    let self = this;
    console.log(e)
    wx.showLoading({
      title: '加载中'
    });
    wx.getUserInfo({
      success(getUserInfoRes) {
        wx.login({
          success(res) {
            console.log(res)
            app.globalData.userInfo = getUserInfoRes.userInfo
            let url = app.globalData.Host + '/login/BindWeChat';
            let data = {
              code: res.code,
              userName: getUserInfoRes.userInfo.nickName,
              avatarUrl: getUserInfoRes.userInfo.avatarUrl,
              userId: self.data.resData.id,
              type: self.data.userType
            }
            app.globalData.post(url, data,
              function (res) {
                let resData = res.data;
                self.setData({
                  resData: resData
                })
                wx.setStorage({
                  key: 'user',
                  data: resData
                })
                app.globalData.userInfo = resData;
                wx.redirectTo({
                  url: '/pages/index/index',
                })
              },
              function (err) {
              }
            )
          },
          fail(err) {
            console.log(err)
            wx.hideLoading({});
            wx.showToast({
              title: '绑定微信失败',
              icon: 'none',
              duration: 1500
            })
          }
        })
      },
      fail(err) {
        console.log(err)
        wx.hideLoading({});
        wx.showToast({
          title: '授权失败',
          icon: 'none',
          duration: 1500
        })
      }
    })
  },
  changePhonenumber: function (e) {
    this.setData({
      phonenumber: e.detail.value
    })
  },
  changeCode: function (e) {
    this.setData({
      code: e.detail.value
    })
  },
  countdownStart: function () {
    let self = this;
    self.setData({
      isCountdown: true
    })
    self.data.setInter = setInterval(function () {
      if (self.data.count > 0) {
        self.setData({
          count: self.data.count - 1
        })
      } else {
        clearInterval(self.data.setInter);
        self.setData({
          isCountdown: false,
          count: 30
        })
      }
    }, 1000)
  },
  hideZqmodal: function () {
    let self = this;
    self.setData({ showZqmodal: false });
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})