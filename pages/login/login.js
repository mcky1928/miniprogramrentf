// pages/login/login.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userType: '1',
    showZqmodal: true,
    resData: {},
    wxcode: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.wxLogin();
  },
  wxLogin: function () {
    let self = this;
    wx.login({
      success(res) {
        console.log(res)
        self.setData({
          code: res.code
        })
      },
      fail(err) {
        console.log(err)
      }
    })
  },
  getUserInfo: function (e) {
    let self = this;
    console.log(e)
    wx.showLoading({
      title: '加载中'
    });
    wx.getUserInfo({
      success(getUserInfoRes) {
        app.globalData.userInfo = getUserInfoRes.userInfo
        let url = app.globalData.Host + '/login/doLoginThird';
        let data = {
          code: self.data.code,
          userName: getUserInfoRes.userInfo.nickName,
          avatarUrl: getUserInfoRes.userInfo.avatarUrl,
          type: self.data.userType
        }
        app.globalData.post(url, data,
          function (res) {
            let resData = res.data;
            self.setData({
              resData: res.data
            })
            if (resData.phone) {
              wx.setStorage({
                key: 'user',
                data: resData
              })
              app.globalData.userInfo = resData;
              wx.redirectTo({
                url: '/pages/index/index',
              })
            } else {
              self.setData({ showZqmodal: true });
            }
          },
          function (err) {
          }
        )
      },
      fail(err) {
        console.log(err)
        wx.hideLoading({});
        wx.showToast({
          title: '登录失败',
          icon: 'none',
          duration: 1500
        })
      },
      fail(err) {
        console.log(err)
        wx.hideLoading({});
        wx.showToast({
          title: '授权失败',
          icon: 'none',
          duration: 1500
        })
      }
    })
  },
  getPhoneNumber: function (e) {
    let self = this;
    console.log(e.detail.errMsg)
    console.log(e.detail.iv)
    console.log(e.detail.encryptedData)
    let url = app.globalData.Host + '/login/WeChatBindPhone';
    let data = {
      sessionKey: self.data.resData.sessionKey,
      ivData: e.detail.iv,
      encrypData: e.detail.encryptedData,
      userId: self.data.resData.id,
      type: self.data.userType
    }
    app.globalData.post(url, data,
      function (res) {
        let resData = res.data;
        self.setData({
          resData: resData
        })
        wx.setStorage({
          key: 'user',
          data: resData
        })
        app.globalData.userInfo = resData;
        wx.redirectTo({
          url: '/pages/index/index',
        })
      },
      function (err) {
      }
    )
  },
  goPhoneLogin: function () {
    wx.navigateTo({
      url: '/pages/login/phoneLogin',
    })
  },
  hideZqmodal: function () {
    let self = this;
    self.setData({ showZqmodal: false });
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.hideZqmodal();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})