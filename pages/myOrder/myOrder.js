// pages/myOrder/myOrder.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    selectedTab: 1, //1 进行中 2 已完成
    count: '20',
    orderList: {
      progress: [],
      finish: []
    },
    userInfo: false,
    List1: [],
    List2: [],
    //上拉加载 下拉刷新
    boxHeight: 0,
    page: 1,
    pageSize: 10,
    loadAll: false,
    scrolltoupper: false,
    scrolltolower: false,
    isTop: true,
    touchStartY: 0,
    touchMoveHeight: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let self = this;
    let userInfo = app.globalData.userInfo ? app.globalData.userInfo : false;
    self.setData({
      userInfo: userInfo,
      page: 1,
      loadAll: false
    })
    self.findRentByUserId();
    self.getConsumerHistoryOrdersByUserId();
  },
  //分页消费者查询正在租用的设备列表
  findRentByUserId: function () {
    let self = this;
    self.setData({
      scrolltoupper: false,
      scrolltolower: false,
      touchMoveHeight: 0
    })
    let url = app.globalData.Host + '/consumer/findRentByUserId';
    let userId = self.data.userInfo.id;
    let page = self.data.page;
    let pageSize = self.data.pageSize;
    let data = {
      userId: userId,
      curPage: page,
      pageSize: pageSize
    }
    app.globalData.post(url, data,
      function (res) {
        let resData = res.data;
        var arr = (page > 1) ? self.data.List1 : [];
        if (resData.length > 0) {
          for (var i = 0; i < resData.length; i++) {
            arr.push(resData[i])
          }
          self.setData({
            page: page + 1,
            scrolltoupper: true,
            scrolltolower: true,
          })
          if (resData.length < pageSize) {
            self.setData({
              loadAll: true
            })
          }
        } else {
          self.setData({
            loadAll: true,
            scrolltoupper: true,
            scrolltolower: true,
          })
        }
        self.setData({
          orderList: {
            progress: arr,
            finish: self.data.orderList.progress,
          }
        })
        self.dealData(arr, '1');
      },
      function (err) {
        self.setData({
          loadAll: false,
          scrolltoupper: true,
          scrolltolower: true,
        })
      }, true
    )
  },
  //分页查询消费者查询历史订单
  getConsumerHistoryOrdersByUserId: function () {
    let self = this;
    self.setData({
      scrolltoupper: false,
      scrolltolower: false,
      touchMoveHeight: 0
    })
    let url = app.globalData.Host + '/consumer/getConsumerHistoryOrdersByUserId';
    let userId = self.data.userInfo.id;
    let page = self.data.page;
    let pageSize = self.data.pageSize;
    let data = {
      userId: userId,
      curPage: page,
      pageSize: pageSize
    }
    app.globalData.post(url, data,
      function (res) {
        let resData = res.data;
        var arr = (page > 1) ? self.data.List2 : [];
        if (resData.length > 0) {
          for (var i = 0; i < resData.length; i++) {
            arr.push(resData[i])
          }
          self.setData({
            page: page + 1,
            scrolltoupper: true,
            scrolltolower: true,
          })
          if (resData.length < pageSize) {
            self.setData({
              loadAll: true
            })
          }
        } else {
          self.setData({
            loadAll: true,
            scrolltoupper: true,
            scrolltolower: true,
          })
        }
        self.setData({
          orderList: {
            progress: self.data.orderList.progress,
            finish: arr,
          }
        })
        self.dealData(arr,'2');
      },
      function (err) {
        self.setData({
          loadAll: false,
          scrolltoupper: true,
          scrolltolower: true,
        })
      }, true
    )
  },
  //整理查询的数据
  dealData: function (list, which) {
    let self = this;
    let arr = Array(0);
    for (let i = 0; i < list.length; i++) {
      let timestamp = which == '1' ? list[i].createTime : list[i].startTime;
      let dateFormat = self.formatTime(timestamp);
      let year = dateFormat.substr(0, 4);
      let date = dateFormat.substr(5, 5);
      let time = dateFormat.substr(11, 5);
      list[i].time = time;
      list[i].spend = Number(list[i].spend)
      list[i].expense = Number(list[i].expense)
      if (list[i].repairs){
        list[i].repairs.claim = Number(list[i].repairs.claim)
      }
      for (let j = 0; j < arr.length + 1; j++) {
        if (j == arr.length) {
          let obj = {
            year : year,
            list: [
              {
                date: date,
                item: Array(list[i])
              },
            ]
          }
          arr.push(obj); 
          break;
        } else {
          if (arr[j].year == year) {
            let arr1 = arr[j].list;
            for (let k = 0; k < arr1.length + 1; k++) {
              if (k == arr1.length) {
                let obj1 = {
                  date: date,
                  item: Array(list[i])
                }
                arr1.push(obj1);
                break;
              } else {
                if (arr1[k].date == date) {
                  arr1[k].item.push(list[i]);
                  break;
                }
              }
            }
            arr[j].list = arr1;
            break;
          }
        }
      }
    }
    if(which == '1') {
      self.setData({
        List1: arr
      })
    } else if (which == '2') {
      self.setData({
        List2: arr
      })
    }
    //console.log(arr)
  },
  bindscroll: function (e) {
    //console.log(e)
    let self = this;
    self.setData({
      isTop: false
    })

  },
  touchStart: function (e) {
    //console.log(e)
    let self = this;
    self.setData({
      touchStartY: e.changedTouches[0].pageY,
      isTop: true
    })
  },
  touchMove: function (e) {
    //console.log(e)
    let self = this;
    let touchStartY = self.data.touchStartY;
    let touchMoveY = e.changedTouches[0].pageY;
    self.setData({
      touchMoveHeight: touchMoveY - touchStartY
    })
  },
  touchEnd: function (e) {
    //console.log(e)
    let self = this;
    let isTop = self.data.isTop;
    let touchStartY = self.data.touchStartY;
    let touchEndY = e.changedTouches[0].pageY;
    //console.log(isTop)
    //console.log(touchStartY)
    //console.log(touchEndY)
    if (touchEndY > touchStartY && isTop) {
      self.myPullDownRefresh();
    }
  },
  myPullDownRefresh: function () {
    let self = this;
    var scrolltoupper = self.data.scrolltoupper;
    if (scrolltoupper) {
      self.onLoad();
    }
  },
  lower(e) {
    //console.log(e)
    let self = this;
    var scrolltolower = self.data.scrolltolower;
    var loadAll = self.data.loadAll;
    if (scrolltolower && !loadAll) {
      self.getConsumerHistoryOrdersByUserId();
    }
  },
  selectTab: function (e) {
    let self = this;
    let selected = e.currentTarget.dataset.select;
    self.setData({
      selectedTab: selected
    })
  },
  computeScrollViewHeight() {
    let that = this
    let query = wx.createSelectorQuery().in(this)
    query.select('.header-tabs').boundingClientRect()
    query.exec(res => {
      let h1 = res[0].height
      let windowHeight = wx.getSystemInfoSync().windowHeight
      let scrollHeight = windowHeight - h1
      this.setData({ boxHeight: scrollHeight })
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.computeScrollViewHeight();
  },
  orderDetails: function(e) {
    let self = this;
    let orderList = self.data.orderList;
    let ordernum = e.currentTarget.dataset.ordernum;
    for (var i = 0; i < orderList.progress.length; i++ ){
      if (ordernum == orderList.progress[i].ordernum){
        app.globalData.orderDetails = orderList.progress[i];
      }
    }
    for (var i = 0; i < orderList.finish.length; i++) {
      if (ordernum == orderList.finish[i].ordernum) {
        app.globalData.orderDetails = orderList.finish[i];
      }
    }
    wx.navigateTo({
      url: '/pages/myOrderDetails/myOrderDetails?ordernum=' + ordernum,
    })
  },
  // 时间格式化
  add0: function (m) { return m < 10 ? '0' + m : m },
  formatTime :function (timestamp) {
    //shijianchuo是整数，否则要parseInt转换
    var time = new Date(timestamp);
    var y = time.getFullYear();
    var m = time.getMonth() + 1;
    var d = time.getDate();
    var h = time.getHours();
    var mm = time.getMinutes();
    var s = time.getSeconds();
    return y + '-' + this.add0(m) + '-' + this.add0(d) + ' ' + this.add0(h) + ':' + this.add0(mm) + ':' + this.add0(s);
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})