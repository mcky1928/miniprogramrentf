// pages/wallet/wallet.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: false,
    accountBalance: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let self = this;
    let userInfo = app.globalData.userInfo ? app.globalData.userInfo : false;
    self.setData({
      userInfo: userInfo,
    })
    // self.getConsumerAccountBalance(userInfo.id);
  },
  recharge: function () {
    wx.navigateTo({
      url: '/pages/recharge/recharge',
    })
  },
  withdrawal: function () {
    wx.navigateTo({
      url: '/pages/withdrawal/withdrawal?accountBalance=' + this.data.accountBalance,
    }) 
  }, 
  //总余额
  getConsumerAccountBalance: function (userId) {
    let self = this;
    let url = app.globalData.Host + '/consumer/getConsumerAccountBalance';
    let data = { userId: userId };
    app.globalData.post(url, data,
      function (res) {
        self.setData({
          accountBalance: res.data,
        })
      },
      function (err) {
      }, true
    )
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let self = this;
    let userInfo = self.data.userInfo;
    self.getConsumerAccountBalance(userInfo.id);
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})