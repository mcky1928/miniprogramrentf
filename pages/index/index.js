//index.js
//获取应用实例
const app = getApp()
Page({
  data: {
    isPX: app.globalData.isPX ? true : false,
    searching: false,//是否在搜索
    userInfo: false,
    boxHeight:'',
    latitude: '',
    longitude: '',
    centerData: {},
    markers:[]
  },
  onLoad: function () {
    let self = this;
    let userInfo = app.globalData.userInfo ? app.globalData.userInfo : false;
    self.setData({
      userInfo: userInfo
    })
    //self.showMap();
    app.globalData.again_getLocation(self);
    self.getSite();

    
   
    /*wx.request({
      url: 'https://api.weixin.qq.com/sns/userinfo?access_token=21__ZITt-pTIu35I3DeA4p6nFbUK6_BxbDhQZlEReLpgYPTxLkTR6_kUjOZsjeU1sJaS7856jk2lOboVFa-MkbgMMB4xBptW50YgQ6dE8iw2EiSzxsyS-EjMlnZnfFSH0SlryDrVyn-jl0W_z4IGZFeACATMS&openid=oNGOQ4iRKH4mhYAoMc6NzLgKsGt4',
      data: {},
      header: {
        'content-type': 'application/json' // 默认值
      },
      success(res) {
        console.log(res)

      },
      fail(err) {
        console.log(err);

      }
    })*/ 

  },
  //定位
  getLocation: function () {
    let self = this;
    wx.showLoading({
      title: '加载中'
    });
    wx.getLocation({
      type: 'wgs84',
      success(res) {
        //console.log(res)
        wx.hideLoading({});
        const latitude = res.latitude
        const longitude = res.longitude
        self.setData({
          latitude: latitude,
          longitude: longitude,
        })
      },
      fail(err) {
        //console.log(err)
        wx.hideLoading({});
        wx.showModal({
          title: '定位失败',
          content: '未获取到您的地理位置，暂时无法为你提供服务。请检查是否已关闭定位权限，或尝试重新打开小程序',
          cancelText: '取消',
          showCancel: false,
          confirmText: '确定',
          confirmColor: '#3095F9',
          success(res) {
            if (res.confirm) {
              //console.log('用户点击确定')
            } else if (res.cancel) {
              //console.log('用户点击取消')
            }
          }
        })
      }
    })
  },
  getSite: function () {
    wx.hideLoading({}); 
    let self = this;
    let openid = self.data.userInfo.uuid;
    let url = app.globalData.Host + '/shop/selectAllShops';
    let data = {
    }
    app.globalData.get(url, data,
      function (res) {
        //console.log(res)
        let markers = res.data;
        markers = markers.map(function(item){
          item.iconPath = '/images/markers.png';
          item.width = '20';
          item.height = '20';
          item.showItem = true;
          item.callout = {
            content: item.shopName,
            padding: 8,
            borderRadius: 5,
            borderColor: '#fefefe',
            bgColor: '#fefefe',
          }
          if(item.checked == 1) {
            return item;
          }
        })
        self.setData({
          markers: markers
        })
      },
      function (err) {
      }, true
    )
  },
  // 点击markers店铺
  callouttap: function (e) {
    //console.log(e)
    let markers = this.data.markers;
    let id = e.markerId;
    var obj = markers.find(function (x) {
      return x.id == id
    })
    wx.openLocation({//​使用微信内置地图查看位置。
      latitude: Number(obj.latitude),//要去的纬度-地址
      longitude: Number(obj.longitude),//要去的经度-地址
      name: obj.shopName,
      address: obj.city + obj.address
    })
  },
  //开锁
  scanCode: function () {
    let self = this;
    wx.scanCode({
      success(res) {
        console.log(res.result)
        // console.log(JSON.parse(res.result))
        // var codeData = JSON.parse(res.result);
        var equId = self.getUrlParam(res.result, 'uuid')
        // var equId = codeData.uuid;
        wx.navigateTo({
          url: '/pages/shop/shop?equId=' + equId,
        })
      }, fail(err) {
        console.log(err)
        wx.showToast({
          title: '扫描失败',
          icon: 'none',
          duration: 1500
        })
      }
    })
  },
  //选中店铺
  selectStore: function (e) {
    //console.log(e)
    let id = e.currentTarget.dataset.id;
    let markers = this.data.markers;
    var obj = markers.find(function (x) {
      return x.id == id
    })
    wx.openLocation({//​使用微信内置地图查看位置。
      latitude: Number(obj.latitude),//要去的纬度-地址
      longitude: Number(obj.longitude),//要去的经度-地址
      name: obj.shopName,
      address: obj.city + obj.address
    })
  },
  //搜索输入
  keyChange: function (e) {
    //console.log(e)
    let self = this;
    let key = e.detail.value;
    let markers = self.data.markers;
    markers = markers.map(function (item) {
      if (item.shopName.indexOf(key)>-1){
        item.showItem = true;
      }else {
        item.showItem = false;
      }
      return item;
    })
    self.setData({
      markers: markers
    })
  },
  //打开搜索
  openSearchBox: function () {
    this.setData({
      searching: true
    })
  },
  //关闭搜索
  closeSearchBox: function () {
    this.setData({
      searching: false
    })
  },
  goPersonal: function (){
    wx.navigateTo({
      url: '/pages/personal/personal',
    })
  },
  computeScrollViewHeight() {
    let that = this
    let query = wx.createSelectorQuery().in(this)
    query.select('.openEqu').boundingClientRect()
    query.select('.pxbottom').boundingClientRect()
    query.exec(res => {
      let h1 = res[0].height
      let h2 = res[1].height
      let windowHeight = wx.getSystemInfoSync().windowHeight
      let scrollHeight = windowHeight - h1 - h2
      this.setData({ boxHeight: scrollHeight })
    })
  },
  //扫码获取参数
  getUrlParam: function (url, name) {
    let arrObj = url.split("?");
    if (arrObj.length > 1) {
      let arrPara = arrObj[1].split("&");
      let arr;
      for (let i = 0; i < arrPara.length; i++) {
        arr = arrPara[i].split("=");
        if (arr != null && arr[0] == name) {
          return arr[1];
        }
      }
      return "";
    } else {
      return "";
    }
  },
  onReady() {
    this.computeScrollViewHeight();
  },
})
