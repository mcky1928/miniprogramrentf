// pages/myOrderDetails/myOrderDetails.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderDetails: {
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    //console.log(options.ordernum)
    let self = this;
    self.setData({
      orderDetails: app.globalData.orderDetails
    })
  },
  copyFun: function () {
    wx.setClipboardData({
      data: this.data.orderDetails.ordernum,
      success(res) {
        //wx.hideToast(); //隐藏复制成功的弹窗提示,根据需求可选
      }
    })

  },
  callPhone: function () {
    wx.makePhoneCall({
      phoneNumber: this.data.orderDetails.phone,
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})