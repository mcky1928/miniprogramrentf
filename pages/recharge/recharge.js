// pages/recharge/recharge.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    money:'',
    userInfo: false,
    userType: '1',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let self = this;
    let userInfo = app.globalData.userInfo ? app.globalData.userInfo : false;
    self.setData({
      userInfo: userInfo,
    })
  },

  moneyChange: function (e) {
    //console.log('金额', e.detail.value)
    var money;
    if (/^(\d?)+(\.\d{0,2})?$/.test(e.detail.value)) { //正则验证，提现金额小数点后不能大于两位数字
      money = e.detail.value;
    } else {
      money = e.detail.value.substring(0, e.detail.value.lastIndexOf('.') + 3);
      wx.showToast({
        title: '充值金额最多两位小数',
        icon: 'none',
        duration: 1500
      })
    }
    this.setData({
      money: money
    })
  },
  recharge: function () {
    let self = this;
    let openid = self.data.userInfo.uuid;
    let money = self.data.money;
    let url = app.globalData.Host + '/WeiXin/wxPay';
    let data = {
      openid: openid,
      money: money,
      type: self.data.userType
    }
    app.globalData.post(url, data,
      function (res) {
        console.log(res)
        self.payment(res.data)
      },
      function (err) {
      }, true
    )
  },
  payment: function (data) {
    var self = this;
    wx.requestPayment({
      timeStamp: data.timeStamp,
      nonceStr: data.nonceStr,
      package: data.package,
      signType: 'MD5',
      paySign: data.paySign,
      success: function (res) {
        // success
        //console.log(res);
        wx.navigateBack({
          delta: 1,
          success: function() {
            wx.showToast({
              title: '充值成功',
              icon: 'none',
              duration: 1500
            })
          }
        })
      },
      fail: function (res) {
        // fail
        //console.log(res);
        wx.navigateBack({
          delta: 1,
          success: function () {
            wx.showToast({
              title: '充值失败',
              icon: 'none',
              duration: 1500
            })
          }
        })
      },
      complete: function (res) {
        // complete
        //console.log(res);
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})