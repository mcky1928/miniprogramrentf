// pages/shopList/shopList.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    keyword: '',
    boxHeight: 0,
    //查询商品列表
    shopId: '',
    user: {},
    page: 1,
    pageSize: 10,
    loadAll: false,
    list: [
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let self = this;
    let shopId = options.shopId;
    self.setData({
      shopId: shopId,
      page: 1,
      loadAll: false
    })
    self.findAllByShopID();
  },
  findAllByShopID: function () {
    let self = this;
    let url = app.globalData.Host + '/equipment/findAllByShopID';
    let shopId = self.data.shopId;
    let page = self.data.page;
    let pageSize = self.data.pageSize;
    let data = {
      shopID: shopId,
      curPage: page,
      pageSize: pageSize
    }
    app.globalData.post(url, data,
      function (res) {
        let resData = res.data;
        var arr = (page > 1) ? self.data.list : [];
        if (resData.length > 0) {
          for (var i = 0; i < resData.length; i++) {
            arr.push(resData[i])
          }
          self.setData({
            page: page + 1,
            list: arr,
          })
          if (resData.length < pageSize) {
            self.setData({
              loadAll: true
            })
          }
        } else {
          self.setData({
            list: arr,
            loadAll: true,
          })
        }
        self.showorhide(self.data.keyword)
      },
      function (err) {
        self.setData({
          loadAll: false,
        })
      }, true
    )
  },
  keywordChange: function(e) {
    let self = this;
    //console.log('keyword', e.detail.value)
    let keyword = e.detail.value;
    self.showorhide(keyword);
  },
  showorhide: function (keyword) {
    let self = this;
    //console.log('keyword', e.detail.value)
    let list = self.data.list;
    list.map(function (item) {
      let str = item.type + '    ' + item.version;
      if (str.indexOf(keyword) > -1) {
        item.showItem = true;
      } else {
        item.showItem = false;
      }
      return item;
    })
    this.setData({
      keyword: keyword,
      list: list
    })
  },
  shop: function (e) {
    console.log(e)
    let equId = e.currentTarget.dataset.equid;
    wx.redirectTo({
      url: '/pages/shop/shop?equId=' + equId,
    })
  },
  computeScrollViewHeight() {
    let that = this
    let query = wx.createSelectorQuery().in(this)
    query.select('.search').boundingClientRect()
    query.exec(res => {
      let h1 = res[0].height
      let windowHeight = wx.getSystemInfoSync().windowHeight
      let scrollHeight = windowHeight - h1
      this.setData({ boxHeight: scrollHeight })
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.computeScrollViewHeight();
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})