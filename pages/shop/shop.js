// pages/shop/shop.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    Host: app.globalData.Host,
    resData: {},
    shopImg: [],
    indicatorDots: true,
    autoplay: false,
    circular: true,
    interval: 5000,
    duration: 1000,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let equId = options.equId;
    this.ewmEquipmentMsg(equId);
  },
  ewmEquipmentMsg: function (equId) {
    let self = this;
    let url = app.globalData.Host + '/consumer/ewmEquipmentMsg';
    let data = {
      uuid: equId
    }
    app.globalData.post(url, data,
      function (res) {
        let resData = res.data;
        let shopImg = [];
        if (res.data.Kinds.url){
          shopImg = res.data.Kinds.url.split(',');
        }
        self.setData({
          resData: resData,
          shopImg: shopImg
        })
        app.globalData.orderInfo = resData;
      },
      function (err) {
      },true
    )
  },
  // 开锁
  unlock: function () {
    wx.navigateTo({
      url: '/pages/payPage/payPage',
    })
  },
  //店铺信息
  storeInfo: function () {
    wx.navigateTo({
      url: '/pages/storeInfo/storeInfo',
    })
  },
  //moreShop
  moreShop: function () {
    let shopId = this.data.resData.Shop.id;
    wx.navigateTo({
      url: '/pages/shopList/shopList?shopId=' + shopId,
    })

  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})