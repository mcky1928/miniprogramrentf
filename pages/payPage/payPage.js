// pages/payPage/payPage.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    Host: app.globalData.Host,
    userType: '1',
    userInfo: false,
    agreePrivacyChecked: true, //同意租赁协议
    orderInfo: {},
    shopImg: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let self = this;
    let userInfo = app.globalData.userInfo ? app.globalData.userInfo : false;
    let orderInfo = app.globalData.orderInfo;
    let shopImg = [];
    if (orderInfo.Kinds.url) {
      shopImg = orderInfo.Kinds.url.split(',');
    }
    self.setData({
      userInfo: userInfo,
      orderInfo: orderInfo,
      shopImg: shopImg
    })
  },
  toggleChecked: function (){
    this.setData({
      agreePrivacyChecked: !this.data.agreePrivacyChecked
    })
  },
  // 租赁协议
  rentPrivacy: function () {
    wx.navigateTo({
      url: '/pages/rentPrivacy/rentPrivacy',
    })
  },
  //确认
  payButton: function (e) {
    let self = this;
    let shopName = self.data.orderInfo.Shop.shopName;
    let deposit = e.currentTarget.dataset.deposit;
    wx.showModal({
      title: '支付',
      content: shopName + '\r\n' + deposit + '元',
      cancelText: '取消',
      showCancel: true,
      confirmText: '确定',
      confirmColor: '#3095F9',
      success(res) {
        if (res.confirm) {
          let obj = {
            uuid: self.data.orderInfo.Equipment.uuid,
            userId: self.data.userInfo.id,
            status: '1',
            data: 'start'
          }
          self.openEqu(obj);
        } else if (res.cancel) {
          //console.log('用户点击取消')
        }
      }
    })
  },
  openEqu: function (obj) {
    let self = this;
    let url = app.globalData.Host + '/consumer/orderEquipment';
    let data = obj;
    app.globalData.post(url, data,
      function (res) {
        if (res.code == '1001') {
          wx.showToast({
            title: '开机成功',
            icon: 'none',
            duration: 1500
          })
          setTimeout(function () {
            wx.reLaunch({
              url: '/pages/index/index',
            })
          }, 1500)
          
        } else {
          wx.showToast({
            title: res.message,
            icon: 'none',
            duration: 2000
          })
        }
      },
      function (err) {
      }, true
    )
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})