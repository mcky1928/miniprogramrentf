// pages/notWithdrawal/notWithdrawal.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: false,
    List:[],
    cantWithdrawal: 0,
    //上拉加载 下拉刷新
    boxHeight: 0,
    page: 1,
    pageSize: 100,
    loadAll: false,
    scrolltoupper: false,
    scrolltolower: false,
    isTop: true,
    touchStartY: 0,
    touchMoveHeight: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let self = this;
    let userInfo = app.globalData.userInfo ? app.globalData.userInfo : false;
    self.setData({
      userInfo: userInfo,
      page: 1,
      loadAll: false
    })
    self.getConsumerCountDeposit(userInfo.id);
    self.getConsumerShallNotWithdraw();
  },
  //不可提现金额
  getConsumerCountDeposit: function (userId) {
    let self = this;
    let url = app.globalData.Host + '/consumer/getConsumerCountDeposit';
    let data = { userId: userId };
    app.globalData.post(url, data,
      function (res) {
        self.setData({
          cantWithdrawal: res.data,
        })
      },
      function (err) {
      }, true
    )
  },
  getConsumerShallNotWithdraw: function () {
    let self = this;
    self.setData({
      scrolltoupper: false,
      scrolltolower: false,
      touchMoveHeight: 0
    })
    let url = app.globalData.Host + '/consumer/getConsumerShallNotWithdraw';
    let userId = self.data.userInfo.id;
    let page = self.data.page;
    let pageSize = self.data.pageSize;
    let data = {
      userId: userId,
      curPage: page,
      pageSize: pageSize
    }
    app.globalData.post(url, data,
      function (res) {
        let resData = res.data;
        var arr = (page > 1) ? self.data.List : [];
        if (resData.length > 0) {
          for (var i = 0; i < resData.length; i++) {
            arr.push(resData[i])
          }
          self.setData({
            page: page + 1,
            List: arr,
            scrolltoupper: true,
            scrolltolower: true,
          })
          if (resData.length < pageSize) {
            self.setData({
              loadAll: true
            })
          }
        } else {
          self.setData({
            List: arr,
            loadAll: true,
            scrolltoupper: true,
            scrolltolower: true,
          })
        }
      },
      function (err) {
        self.setData({
          loadAll: false,
          scrolltoupper: true,
          scrolltolower: true,
        })
      }, true
    )
  },
  bindscroll: function (e) {
    //console.log(e)
    let self = this;
    self.setData({
      isTop: false
    })

  },
  touchStart: function (e) {
    //console.log(e)
    let self = this;
    self.setData({
      touchStartY: e.changedTouches[0].pageY,
      isTop: true
    })
  },
  touchMove: function (e) {
    //console.log(e)
    let self = this;
    let touchStartY = self.data.touchStartY;
    let touchMoveY = e.changedTouches[0].pageY;
    self.setData({
      touchMoveHeight: touchMoveY - touchStartY
    })
  },
  touchEnd: function (e) {
    //console.log(e)
    let self = this;
    let isTop = self.data.isTop;
    let touchStartY = self.data.touchStartY;
    let touchEndY = e.changedTouches[0].pageY;
    //console.log(isTop)
    //console.log(touchStartY)
    //console.log(touchEndY)
    if (touchEndY > touchStartY && isTop) {
      self.myPullDownRefresh();
    }
  },
  myPullDownRefresh: function () {
    let self = this;
    var scrolltoupper = self.data.scrolltoupper;
    if (scrolltoupper) {
      self.onLoad();
    }
  },
  lower(e) {
    //console.log(e)
    let self = this;
    var scrolltolower = self.data.scrolltolower;
    var loadAll = self.data.loadAll;
    if (scrolltolower && !loadAll) {
      self.getConsumerShallNotWithdraw();
    }
  },
  computeScrollViewHeight() {
    let that = this
    let query = wx.createSelectorQuery().in(this)
    query.select('.title').boundingClientRect()
    query.exec(res => {
      let h1 = res[0].height
      let windowHeight = wx.getSystemInfoSync().windowHeight
      let scrollHeight = windowHeight - h1
      this.setData({ boxHeight: scrollHeight })
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.computeScrollViewHeight();
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})