// pages/withdrawal/withdrawal.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userType: '1',
    userInfo: false,
    accountBalance: 0,
    cantWithdrawal: 0,
    money: null,
    // 支付
    showZqmodal: false,
    pwdLength: 6,    //输入框个数 
    isFocus: false,  //聚焦 
    inputPwd: "",    //输入的内容 
    ispassword: true, //是否密文显示 true为密文， false为明文
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let self = this;
    let accountBalance = options.accountBalance; 
    let userInfo = app.globalData.userInfo ? app.globalData.userInfo : false;
    self.setData({
      userInfo: userInfo,
      accountBalance: accountBalance
    }) 
    self.getConsumerCountDeposit(userInfo.id);
  },
  //不可提现金额
  getConsumerCountDeposit: function (userId) {
    let self = this;
    let url = app.globalData.Host + '/consumer/getConsumerCountDeposit';
    let data = { userId: userId };
    app.globalData.post(url, data,
      function (res) {
        self.setData({
          cantWithdrawal: res.data,
        })
      },
      function (err) {
      }, true
    )
  },
  moneyChange: function (e) {
    //console.log('金额', e.detail.value)
    var money;
    if (/^(\d?)+(\.\d{0,2})?$/.test(e.detail.value)) { //正则验证，提现金额小数点后不能大于两位数字
      money = e.detail.value;
    } else {
      money = e.detail.value.substring(0, e.detail.value.lastIndexOf('.') + 3);
      wx.showToast({
        title: '提现金额最多两位小数',
        icon: 'none',
        duration: 1500
      })
    }
    this.setData({
      money: money
    })
  },
  withdrawal: function () {
    let self = this;
    let accountBalance = self.data.accountBalance;
    let money = self.data.money;
    if (money <= 0) {
      wx.showToast({
        title: '请输入大于0的提现金额',
        icon: 'none',
        duration: 1500
      })
      return;
    }
    if (accountBalance - money < 0) {
      wx.showToast({
        title: '超出本次可提现余额',
        icon: 'none',
        duration: 1500
      })
      return;
    }
    wx.showModal({
      title: '提现',
      content: self.data.money + '元',
      cancelText: '取消',
      showCancel: true,
      confirmText: '确定',
      confirmColor: '#3095F9',
      success(res) {
        if (res.confirm) {
          //console.log('用户点击确定')
          self.payment();
        } else if (res.cancel) {
          //console.log('用户点击取消')
        }
      }
    })
  },
  payment: function () {
    let self = this;
    let userId = self.data.userInfo.id;
    let openid = self.data.userInfo.uuid;
    let money = self.data.money;
    let url = app.globalData.Host + '/WeiXin/wxWithdrawCustomer';
    let data = {
      userId: userId,
      openid: openid,
      money: money
    }
    app.globalData.post(url, data,
      function (res) {
        console.log(res)
        wx.showModal({
          title: '提现通知',
          content: '提现申请已提交，目前不支持自动提现功能，1~3个工作日会有客服人员联系您',
          showCancel: false,
          success(res) {
            if (res.confirm) {
              console.log('用户点击确定')
              wx.navigateBack({
                delta:1
              })
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      },
      function (err) {
      }, true
    )
  },
  notWithdrawal: function () {
    wx.navigateTo({
      url: '/pages/notWithdrawal/notWithdrawal',
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})