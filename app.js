//app.js
App({
  onLaunch: function () {
    var self = this;
    wx.getSystemInfo({
      success(res) {
        let model = res.model;
        if (model.indexOf('iPhone X') > -1) {
          self.globalData.isPX = true;
        }
      }
    })
    // 获取用户信息
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo'] != undefined && res.authSetting['scope.userInfo'] != true) {//非初始化进入该页面,且未授权
          wx.showModal({
            title: '微信授权',
            content: '需要获取您的用户信息，请确认授权，否则可能无法正常使用小程序',
            success: function (res) {
              //console.log(res)
              if (res.cancel) {
              } else if (res.confirm) {
                wx.openSetting({
                  success: function (dataAu) {
                    //console.log(dataAu)
                    if (dataAu.authSetting["scope.userInfo"] == true) {
                      wx.showToast({
                        title: '授权成功',
                        icon: 'success',
                        duration: 1000
                      })
                    } else {
                      wx.showToast({
                        title: '授权失败',
                        icon: 'success',
                        duration: 1000
                      })
                    }
                  }
                })
              }
            }
          })
        } else if (res.authSetting['scope.userLocation'] == undefined) {//初始化进入 
        }
        else { //授权后默认加载
        }
      }
    })
    wx.showLoading({
      title: '加载中',
      mask: true
    });
    var user = wx.getStorageSync('user');
    console.log(user)
    if (user) {
      self.globalData.userInfo = user;
      wx.hideLoading({});
    } else {
      setTimeout(function () {
        wx.hideLoading({});
        wx.redirectTo({
          url: '/pages/login/login',
        })
      }, 500)
    }
  },
  globalData: {
    isPX: false,
    userInfo: null,
    Host: 'https://api.chinadive-robot.com',//服务器地址
    orderInfo: {}, //正在下单信息
    orderDetails: {}, //订单详情
    get: function (url, data, successCallback, errCallback, loading) {
      var loading = loading ? loading : false;
      wx.showLoading({
        title: '加载中',
        mask: true
      });
      wx.request({
        url: url,
        data: data,
        header: {
          'content-type': 'application/json' // 默认值
        },
        success(res) {
          console.log(res)
          if (res.statusCode == '200') {
            if (res.data.code == '1001') {
              if (!loading) {
                wx.showToast({
                  title: res.data.message,
                  icon: 'none',
                  duration: 1500
                })
              }
            } else {
              if (!loading) {
                wx.showToast({
                  title: res.data.message,
                  icon: 'none',
                  duration: 1500
                })
              }
            }
            setTimeout(function () {
              wx.hideLoading({});
              successCallback(res.data);
            }, 1500)
          } else {
            wx.showToast({
              title: '加载失败',
              icon: 'none',
              duration: 1500
            })
          }
        },
        fail(err) {
          console.log(err);
          wx.hideLoading({});
          wx.showToast({
            title: '请检查网络',
            icon: 'none',
            duration: 1500
          })
          errCallback(err);
        }
      })
    },
    post: function (url, data, successCallback, errCallback, loading) {
      var loading = loading ? loading : false;
      wx.showLoading({
        title: '加载中',
        mask: true
      });
      wx.request({
        url: url,
        data: data,
        method: 'POST',
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        success(res) {
          console.log(res)
          if (res.statusCode == '200') {
            if (res.data.code == '1001') {
              if (!loading) {
                wx.showToast({
                  title: res.data.message,
                  icon: 'none',
                  duration: 1500
                })
              }
            } else {
              if (!loading) {
                wx.showToast({
                  title: res.data.message,
                  icon: 'none',
                  duration: 1500
                })
              }
            }
            setTimeout(function () {
              wx.hideLoading({});
              successCallback(res.data);
            }, 1500)
          } else {
            wx.showToast({
              title: '加载失败',
              icon: 'none',
              duration: 1500
            })
          }
        },
        fail(err) {
          wx.hideLoading({});
          wx.showToast({
            title: '请检查网络',
            icon: 'none',
            duration: 1500
          })
          console.log(err);
          errCallback(err);
        }
      })
    },
    again_getLocation: function (self) {
      let that = this;
      // 获取位置信息
      wx.getSetting({
        success: (res) => {
          //console.log(res)
          if (res.authSetting['scope.userLocation'] != undefined && res.authSetting['scope.userLocation'] != true) {//非初始化进入该页面,且未授权
            wx.showModal({
              title: '授权当前位置',
              content: '需要获取您的地理位置，请确认授权，否则无法获取您所需数据',
              success: function (res) {
                //console.log(res)
                if (res.cancel) {
                  wx.showModal({
                    title: '定位失败',
                    content: '未获取到您的地理位置，暂时无法为你提供服务。请检查是否已关闭定位权限，或尝试重新打开小程序',
                    cancelText: '取消',
                    showCancel: false,
                    confirmText: '确定',
                    confirmColor: '#3095F9',
                    success(res) {
                      if (res.confirm) {
                        //console.log('用户点击确定')
                      } else if (res.cancel) {
                        //console.log('用户点击取消')
                      }
                    }
                  })
                } else if (res.confirm) {
                  wx.openSetting({
                    success: function (dataAu) {
                     // console.log(dataAu)
                      if (dataAu.authSetting["scope.userLocation"] == true) {
                        wx.showToast({
                          title: '授权成功',
                          icon: 'success',
                          duration: 1000
                        })
                        //再次授权，调用getLocationt的API
                        self.getLocation();
                      } else {
                        wx.showModal({
                          title: '定位失败',
                          content: '未获取到您的地理位置，暂时无法为你提供服务。请检查是否已关闭定位权限，或尝试重新打开小程序',
                          cancelText: '取消',
                          showCancel: false,
                          confirmText: '确定',
                          confirmColor: '#3095F9',
                          success(res) {
                            if (res.confirm) {
                              //console.log('用户点击确定')
                            } else if (res.cancel) {
                              //console.log('用户点击取消')
                            }
                          }
                        })
                      }
                    }
                  })
                }
              }
            })
          } else if (res.authSetting['scope.userLocation'] == undefined) {//初始化进入
            self.getLocation();
          }
          else { //授权后默认加载
            self.getLocation();
          }
        }
      })
    },
  }
})